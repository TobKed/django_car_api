#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

echo
echo "Deploy car api to Heroku "
echo "Be sure you are logged in to heroku and docker. You can do this with commands:"
echo "   $ heroku login"
echo "   $ docker login --username=_ --password=\$(heroku auth:token) registry.heroku.com"
echo

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_DIR="$( cd "${SCRIPT_DIR}/.." && pwd )"


function usage {
    echo "Deploy car api to Heroku."
    echo
    echo "Usage: bash scripts/$(basename $0) [HEROKU_APP]"
    echo
    echo "HEROKU_APP - A name of the Heroku app to which containers will be deployed."
    exit 1
}


if [[ $# -ne 1 ]]; then
    echo "You must provide a project name."
    echo
    usage
fi

HEROKU_APP=$1

cd "${PROJECT_DIR}"

echo "Build heroku-ready local Docker images"
docker build -f ./compose/production/django/Dockerfile . -t djang_car_api_production_heroku
docker tag djang_car_api_production_heroku "registry.heroku.com/${HEROKU_APP}/web"
echo

echo "Push your directory container for the web and worker process to heroku"
docker push "registry.heroku.com/${HEROKU_APP}/web"
echo

echo "Promote the web and worker process with your container"
heroku container:release -a "${HEROKU_APP}" web
echo

echo "Run migrations"
heroku run python manage.py migrate -a "${HEROKU_APP}"
echo


echo "Check deploy"
heroku run python manage.py check --deploy -a "${HEROKU_APP}"
echo

echo "Open app in browser"
heroku open -a "${HEROKU_APP}" /cars/
echo

echo "Finished!"
