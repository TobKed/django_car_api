import pytest

from rest_api.models import Car, Rating
from rest_api.tests.factories import CarFactory, CarWithRatesFactory, RatingFactory


@pytest.fixture
def car() -> Car:
    return CarFactory()


@pytest.fixture
def car_with_rates() -> Car:
    return CarWithRatesFactory()


@pytest.fixture
def rating() -> Rating:
    return RatingFactory()


def pytest_runtest_setup(item):
    include_long_running = item.config.getoption("--external_api")

    if not include_long_running:
        skip_external_api_test(item)


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "external_api: mark test which hits to external api."
    )


def pytest_addoption(parser):
    parser.addoption(
        "--external_api",
        action="store_true",
        help="Includes external api tests (marked with external_api marker). They are skipped by default.",
    )


def skip_external_api_test(item):
    for _ in item.iter_markers(name="external_api"):
        pytest.skip(
            "The test is skipped because it has external_api marker. "
            "And --external_api flag is not passed to pytest. {item}".format(item=item)
        )
