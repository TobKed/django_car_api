# NG Car Api

#### Endpoints

 - `GET /cars/` - no particular order
 - `GET /cars/<car_id>/`
 - `DELETE /cars/<car_id>/`
 - `POST /cars/` - data example: `{"make": "Volkswagen", "model" : "Golf"}`
 - `POST /rate/` - data example: `{"car_id": 1, "rating": 2}`
 - `GET /popular/`

## CI/CD

CI/CD is set for GitHub Actions. Consists of the following workflows:

 - `tests` - unit tests, performed on pull requests commits and pushes to master (additional manual trigger).
 - `precommit` - checks is code pass `precommit`, triggered as `tests` workflow.
 - `heroku` - deploys the app to heroku, triggered only manually.

## Up and Run locally with docker

1. Run application:

   ```
   docker-compose -f local.yml up
   ```

1. Make migrations (if needed):

   ```
   docker-compose -f local.yml run --rm django python manage.py migrate
   ```

1. Apply migrations (if needed):

   ```
   docker-compose -f local.yml run --rm django python manage.py migrate
   ```

1. Run django shell:

   ```
   docker-compose -f local.yml run --rm django python manage.py shell_plus
   ```

1. Run unit tests (without hitting to external api):

   ```
   docker-compose -f local.yml run --rm django pytest -vrs
   ```

1. Run unit tests (with hitting to external api):

   ```
   docker-compose -f local.yml run --rm django pytest -vrs --external_api
   ```

1. You can load sample data:
   ```
   docker-compose -f local.yml run --rm django python manage.py loaddata rest_api/fixtures/sample_data.json
   ```

#### Environment variables

For dockerized application all files which defines environment variables (with examples for production) are in `.envs` folder.

## Up and Run locally with virtualenv

1. Create and activate virtualenv:

   ```
   python3.8 -m venv <virtual env path>
   source <virtual env path>/bin/activate
   ```

1. Install development requirements:

   ```
   requirements/local_venv.txt
   pre-commit install
   ```

1. Set environment variable for postgres database (not described here how to run database locally):

   ```
   export DATABASE_URL=postgres://postgres:<password>@127.0.0.1:5432/<DB name given to createdb>
   ```

1. Make migrations (if needed):

   ```
   python manage.py makemigrations
   ```

1. Apply migrations:

   ```
   python manage.py migrate
   ```

1. Run Django development server:

   ```
   python manage.py runserver_plus 0.0.0.0:8000
   ```

## Manual deployment on Heroku

I assume you already have basic knowledge about Heroku platform.

Application will be deployed in containerized version.

1. Create app on [heroku.com](https://www.heroku.com/) and set variable with its name:

   ```
   export HEROKU_APP=<app_name>
   ```

1. Create postgres database:

   ```
   heroku addons:create heroku-postgresql:hobby-dev
   ```

1. Set environment variables on heroku

   ```
   heroku config:set PYTHONHASHSEED=random

   heroku config:set WEB_CONCURRENCY=4

   heroku config:set DJANGO_DEBUG=False
   heroku config:set DJANGO_SETTINGS_MODULE=django_car_api.settings.production
   heroku config:set DJANGO_SECRET_KEY="$(openssl rand -base64 64)"

   # Generating a 32 character-long random string without any of the visually similar characters "IOl01":
   heroku config:set DJANGO_ADMIN_URL="$(openssl rand -base64 4096 | tr -dc 'A-HJ-NP-Za-km-z2-9' | head -c 32)/"

   # Set this to your Heroku app url, e.g. 'bionic-beaver-28392.herokuapp.com'
   heroku config:set DJANGO_ALLOWED_HOSTS=
   ```

1. Run helper deployment script:

   ```
   bash scripts/deploy.sh
   ```
