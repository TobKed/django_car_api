from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.db.models import Avg, Count


class CarManager(models.Manager):
    """Car manager with custom annotations."""

    def with_avg_rating(self):
        return self.annotate(avg_rating=Avg("ratings__rating"))

    def with_rates_number(self):
        return self.annotate(rates_number=Count("ratings"))


class Car(models.Model):
    """Basic Car model."""

    make = models.CharField(max_length=255)
    model = models.CharField(max_length=255)

    objects = CarManager()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["make", "model"],
                name="Make and model creates unique combination",
            )
        ]

    def __repr__(self):
        return f'{self.__class__.__name__}(make="{self.make}", model="{self.model}")'


class Rating(models.Model):
    """Basic Car Rating model."""

    car_id = models.ForeignKey(Car, on_delete=models.CASCADE, related_name="ratings")
    rating = models.IntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(5)]
    )

    def __repr__(self):
        return (
            f"{self.__class__.__name__}(car_id={self.car_id.pk}, rating={self.rating})"
        )
