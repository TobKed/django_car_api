import random
from unittest import mock

import pytest
import requests
from django.db.models import Avg
from django.test import RequestFactory
from rest_framework import status
from rest_framework.exceptions import ValidationError

from rest_api.models import Car
from rest_api.tests.factories import CarWithRatesFactory
from rest_api.views import CarDetail, CarList, popular_get_view, rate_post_view

pytestmark = pytest.mark.django_db

MAKE_FORMATTED = "Volkswagen"
MAKE_NOT_FORMATTED = "VolKsWagEn"
MODEL_FORMATTED = "Golf"
MODEL_NOT_FORMATTED = "GoLf"


class TestCarCreateView:
    @mock.patch("rest_api.views.get_car_make_and_model_from_NHTSA")
    def test_car_post_view_pass(
        self,
        mock_validate: mock.MagicMock,
        rf: RequestFactory,
    ):
        mock_validate.return_value = (MAKE_FORMATTED, MODEL_FORMATTED)

        view = CarList.as_view()
        request = rf.post(
            "/cars/", data={"make": MAKE_NOT_FORMATTED, "model": MODEL_NOT_FORMATTED}
        )
        response = view(request)

        mock_validate.assert_called_once_with(MAKE_NOT_FORMATTED, MODEL_NOT_FORMATTED)
        assert response.status_code == status.HTTP_201_CREATED
        assert response.data["make"] == MAKE_FORMATTED
        assert response.data["model"] == MODEL_FORMATTED

    @mock.patch("rest_api.views.get_car_make_and_model_from_NHTSA")
    def test_car_post_view_already_exists(
        self, mock_validate: mock.MagicMock, rf: RequestFactory
    ):
        Car.objects.create(make=MAKE_FORMATTED, model=MODEL_FORMATTED)

        view = CarList.as_view()
        request = rf.post(
            "/cars/", data={"make": MAKE_NOT_FORMATTED, "model": MODEL_NOT_FORMATTED}
        )
        response = view(request)

        mock_validate.assert_not_called()
        assert response.status_code == status.HTTP_208_ALREADY_REPORTED
        assert response.data["make"] == MAKE_FORMATTED
        assert response.data["model"] == MODEL_FORMATTED

    @mock.patch("rest_api.views.get_car_make_and_model_from_NHTSA")
    def test_car_post_view_invalid_car(
        self, mock_validate: mock.MagicMock, rf: RequestFactory
    ):
        mock_validate.side_effect = ValidationError

        view = CarList.as_view()
        request = rf.post(
            "/cars/", data={"make": MAKE_NOT_FORMATTED, "model": MODEL_NOT_FORMATTED}
        )
        response = view(request)

        mock_validate.assert_called_once_with(MAKE_NOT_FORMATTED, MODEL_NOT_FORMATTED)
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data == {
            "detail": "Car not found in NHTSA. Please check spelling."
        }

    #
    @pytest.mark.parametrize(
        "exception",
        [requests.HTTPError(), requests.ConnectionError()],
    )
    @mock.patch("rest_api.views.get_car_make_and_model_from_NHTSA")
    def test_car_post_view_request_error(
        self, mock_validate: mock.MagicMock, exception: Exception, rf: RequestFactory
    ):
        mock_validate.side_effect = exception

        view = CarList.as_view()
        request = rf.post(
            "/cars/", data={"make": MAKE_NOT_FORMATTED, "model": MAKE_NOT_FORMATTED}
        )
        response = view(request)

        mock_validate.assert_called_once_with(MAKE_NOT_FORMATTED, MAKE_NOT_FORMATTED)
        assert response.status_code == status.HTTP_500_INTERNAL_SERVER_ERROR
        assert response.data == {"detail": "Could not connect to NHTSA for validation."}


class TestCarListView:
    def test_car_list_view(self, rf: RequestFactory):
        for i in range(random.randint(5, 10)):
            CarWithRatesFactory()

        view = CarList.as_view()
        request = rf.get("/cars/")
        response = view(request)

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == Car.objects.count()
        data = response.data[0]
        assert "id" in data
        assert "make" in data
        assert "model" in data
        assert "avg_rating" in data
        assert len(data) == 4


class TestCarDetailView:
    def test_car_detail_view_exists(self, rf: RequestFactory):
        car = CarWithRatesFactory()
        avg_rating = car.ratings.aggregate(avg=Avg("rating"))["avg"]

        view = CarDetail.as_view()
        request = rf.get("/cars/")
        response = view(request, pk=car.id)

        assert response.status_code == status.HTTP_200_OK
        assert response.data["id"] == car.id
        assert response.data["make"] == car.make
        assert response.data["model"] == car.model
        assert str(response.data["avg_rating"]) == str(round(avg_rating, 1))
        assert len(response.data) == 4

    def test_car_detail_view_not_exists(self, rf: RequestFactory):
        view = CarDetail.as_view()
        request = rf.get("/cars/")
        response = view(request, pk=1234)
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestCarDeleteView:
    def test_car_delete_view(self, rf: RequestFactory):
        car = CarWithRatesFactory()
        view = CarDetail.as_view()
        request = rf.delete("/cars/")
        response = view(request, pk=car.id)
        assert response.status_code == status.HTTP_204_NO_CONTENT

    def test_car_delete_view_not_existing(self, rf: RequestFactory):
        view = CarDetail.as_view()
        request = rf.delete("/cars/")
        response = view(request, pk=1234)
        assert response.status_code == status.HTTP_404_NOT_FOUND


class TestRatingView:
    @pytest.mark.parametrize(
        "rating",
        [i for i in range(1, 6)],
    )
    def test_rate_post_view_pass(self, rf: RequestFactory, rating):
        car = CarWithRatesFactory()

        view = rate_post_view
        request = rf.post("/rate/", data={"car_id": car.id, "rating": rating})
        response = view(request)

        assert response.status_code == status.HTTP_201_CREATED

    @pytest.mark.parametrize(
        "rating",
        [0, -1, 6, 100, -100],
    )
    def test_rate_post_view_wrong_value(self, rf: RequestFactory, rating):
        car = CarWithRatesFactory()

        view = rate_post_view
        request = rf.post("/rate/", data={"car_id": car.id, "rating": rating})
        response = view(request)

        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_rate_post_view_car_not_exist(self, rf: RequestFactory, rating):
        view = rate_post_view
        request = rf.post("/rate/", data={"car_id": 123456, "rating": 5})
        response = view(request)

        assert response.status_code == status.HTTP_400_BAD_REQUEST


class TestPopularView:
    def test_rate_post_view_pass(self, rf: RequestFactory):
        for _ in range(random.randint(5, 15)):
            CarWithRatesFactory()

        cars = Car.objects.with_rates_number().order_by("-rates_number").all()
        rates_raw = [c.rates_number for c in cars]

        view = popular_get_view
        request = rf.get("/pupular/")
        response = view(request)

        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == len(cars)
        assert [c["rates_number"] for c in response.data] == sorted(
            rates_raw, reverse=True
        )
