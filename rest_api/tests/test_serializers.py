import pytest

from rest_api.models import Car, Rating
from rest_api.serializers import CarSerializer, RatingSerializer

pytestmark = pytest.mark.django_db


def test_car_serializer(car: Car):
    serializer = CarSerializer(car)
    assert serializer.data["id"] == car.id
    assert serializer.data["make"] == car.make
    assert serializer.data["model"] == car.model
    assert len(serializer.data) == 3


def test_car_with_avg_rating_serializer(car_with_rates: Car):
    car = Car.objects.with_avg_rating().get(pk=car_with_rates.pk)
    serializer = CarSerializer(car)
    assert serializer.data["id"] == car.id
    assert serializer.data["make"] == car.make
    assert serializer.data["model"] == car.model
    assert str(serializer.data["avg_rating"]) == str(round(car.avg_rating, 1))
    assert len(serializer.data) == 4


def test_car_with_rates_number_serializer(car_with_rates: Car):
    car = Car.objects.with_rates_number().get(pk=car_with_rates.pk)
    car.rates_number = car.ratings.count()
    serializer = CarSerializer(car)
    assert serializer.data["id"] == car.id
    assert serializer.data["make"] == car.make
    assert serializer.data["model"] == car.model
    assert str(serializer.data["rates_number"]) == str(round(car.rates_number, 1))
    assert len(serializer.data) == 4


def test_rate_serializer(rating: Rating):
    serializer = RatingSerializer(rating)
    assert serializer.data["rating"] == rating.rating
    assert serializer.data["car_id"] == rating.car_id.id
    assert len(serializer.data) == 2
