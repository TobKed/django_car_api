import pytest
from django.db.models import Avg

from rest_api.models import Car, Rating

pytestmark = pytest.mark.django_db


def test_car_str(car: Car):
    assert repr(car) == f'Car(make="{car.make}", model="{car.model}")'


def test_car_manager_with_avg_rating(car_with_rates: Car):
    avg_rating = car_with_rates.ratings.aggregate(avg=Avg("rating"))["avg"]
    car_by_custom_manager = Car.objects.with_avg_rating().get(pk=car_with_rates.pk)
    assert car_by_custom_manager.avg_rating == avg_rating


def test_car_manager_with_rates_number(car_with_rates: Car):
    car_by_custom_manager = Car.objects.with_rates_number().get(pk=car_with_rates.pk)
    assert car_by_custom_manager.rates_number == car_by_custom_manager.ratings.count()


def test_ratings_str(rating: Rating):
    assert repr(rating) == f"Rating(car_id={rating.car_id.pk}, rating={rating.rating})"
