import pytest

from rest_api.models import Car, Rating

pytestmark = pytest.mark.django_db


def test_car_str(car: Car):
    assert repr(car) == f'Car(make="{car.make}", model="{car.model}")'


def test_ratings_str(rating: Rating):
    assert repr(rating) == f"Rate(car_id={rating.car_id.pk}, rating={rating.rating})"
