from factory import Faker, SubFactory, fuzzy, post_generation
from factory.django import DjangoModelFactory

from rest_api.models import Car, Rating


class CarFactory(DjangoModelFactory):
    make = Faker("company")
    model = Faker("name")

    class Meta:
        model = Car


class RatingFactory(DjangoModelFactory):
    car_id = SubFactory(CarFactory)
    rating = fuzzy.FuzzyInteger(low=1, high=5)

    class Meta:
        model = Rating


class CarWithRatesFactory(CarFactory):
    @post_generation
    def ratings(obj, create, extracted, **kwargs):
        """
        If called like: CarWithRatingFactory(ratings=4) it generates a Car with 4
        ratings.  If called without `ratings` argument, it generates a
        random amount of ratings for this car
        """
        if not create:
            # Build, not create related
            return

        if extracted:
            for n in range(extracted):
                RatingFactory(car_id=obj)
        else:
            import random

            number_of_units = random.randint(1, 10)
            for n in range(number_of_units):
                RatingFactory(car_id=obj)
