from unittest import mock

import pytest
from rest_framework.exceptions import ValidationError

from rest_api.validators import get_car_make_and_model_from_NHTSA


@pytest.mark.parametrize(
    "model,model_db",
    [("Golf", "Golf"), ("GOLF", "golf"), ("golf", "GOLF")],
)
@mock.patch("rest_api.validators.requests.get")
def test_get_car_make_and_model_from_NHTSA_pass(mock_requests, model, model_db):
    make = "VolKswaGen"

    mock_requests.return_value.json.return_value = {
        "Count": 44,
        "Results": [
            {"Make_Name": "Volkswagen", "Model_Name": model_db},
            {"Make_Name": "Volkswagen", "Model_Name": "Jetta"},
        ],
    }
    result = get_car_make_and_model_from_NHTSA(make, model)
    mock_requests.assert_called_once_with(
        f"https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/{make}?format=json"
    )
    assert result == ("Volkswagen", model_db)


@mock.patch("rest_api.validators.requests.get")
def test_get_car_make_and_model_from_NHTSA_make_not_exists(mock_requests):
    make = "non_existent_make"
    model = "Jetta"

    mock_requests.return_value.json.return_value = {"Count": 0, "Results": []}
    with pytest.raises(ValidationError):
        get_car_make_and_model_from_NHTSA(make, model)
    mock_requests.assert_called_once_with(
        f"https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/{make}?format=json"
    )


@mock.patch("rest_api.validators.requests.get")
def test_get_car_make_and_model_from_NHTSA_model_not_exists(mock_requests):
    make = "Volkswagen"
    model = "non_existent_model"

    mock_requests.return_value.json.return_value = {
        "Count": 44,
        "Results": [
            {"Model_Name": "Transporter"},
            {"Model_Name": "Golf"},
        ],
    }

    with pytest.raises(ValidationError):
        get_car_make_and_model_from_NHTSA(make, model)
    mock_requests.assert_called_once_with(
        f"https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/{make}?format=json"
    )


@pytest.mark.external_api
def test_get_car_make_and_model_from_NHTSA_true_external_api():
    make = "VolkswaGen"
    model = "JeTta"
    result = get_car_make_and_model_from_NHTSA(make, model)
    assert result == ("Volkswagen", "Jetta")


@pytest.mark.external_api
@pytest.mark.parametrize(
    "make,model",
    [("Volkswagen", "non_existent_model"), ("non_existent_model", "Jetta")],
)
def test_get_car_make_and_model_from_NHTSA_false_external_api(make, model):
    with pytest.raises(ValidationError):
        get_car_make_and_model_from_NHTSA(make, model)
