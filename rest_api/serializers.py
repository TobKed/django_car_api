from rest_framework import serializers

from rest_api.models import Car, Rating


class CarSerializer(serializers.ModelSerializer):
    # avr_rating and rates_number are added to car instance as annotated fields
    # when they are not present these fields will be omitted by drf
    # without throwing exception
    avg_rating = serializers.DecimalField(
        max_digits=2, decimal_places=1, read_only=True
    )
    rates_number = serializers.IntegerField(read_only=True)

    class Meta:
        model = Car
        fields = ["id", "make", "model", "avg_rating", "rates_number"]


class RatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        fields = ["car_id", "rating"]
