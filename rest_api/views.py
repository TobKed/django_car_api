import logging

import requests
from django.http import Http404
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_api.models import Car
from rest_api.serializers import CarSerializer, RatingSerializer
from rest_api.validators import get_car_make_and_model_from_NHTSA


class CarList(APIView):
    """List all cars, or create a new car."""

    def get(self, request, format=None):
        cars = Car.objects.with_avg_rating().all()
        serializer = CarSerializer(cars, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CarSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        make = serializer.validated_data["make"]
        model = serializer.validated_data["model"]

        # validating against database is less expensive than hitting external api
        car = Car.objects.filter(
            make__iexact=make,
            model__iexact=model,
        ).first()
        if car:
            serializer = CarSerializer(car)
            return Response(serializer.data, status=status.HTTP_208_ALREADY_REPORTED)

        try:
            make_nhtsa, model_nhtsa = get_car_make_and_model_from_NHTSA(make, model)
        except (requests.HTTPError, requests.ConnectionError):
            logging.exception("Error during NHTSA validation")
            return Response(
                {"detail": "Could not connect to NHTSA for validation."},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        except ValidationError:
            return Response(
                {"detail": "Car not found in NHTSA. Please check spelling."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer.validated_data["make"] = make_nhtsa
        serializer.validated_data["model"] = model_nhtsa

        serializer.save()
        return Response(serializer.validated_data, status=status.HTTP_201_CREATED)


class CarDetail(APIView):
    """Retrieve, update or delete a car instance."""

    def get_object(self, pk):
        try:
            return Car.objects.with_avg_rating().get(pk=pk)
        except Car.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        car = self.get_object(pk)
        serializer = CarSerializer(car)
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        car = self.get_object(pk)
        car.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(["POST"])
def rate_post_view(request):
    """Create a new rate."""
    serializer = RatingSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
def popular_get_view(request):
    """Get cars with the most ratings."""
    cars = Car.objects.with_rates_number().order_by("-rates_number").all()
    serializer = CarSerializer(cars, many=True)
    return Response(serializer.data)
