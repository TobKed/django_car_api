from typing import List, Tuple

import requests
from rest_framework.exceptions import ValidationError


def get_car_make_and_model_from_NHTSA(make: str, model: str) -> Tuple[str, str]:
    """
    Function te get formatted make and model car name from the
    `The National Highway Traffic Safety Administration` database.
    If car exists it returns tuple with formatted make and model names.
        (make is title cased, model is take from database).
    If car not exists rest_framework.exceptions.ValidationError is raised.

    :param make: manufacturer of car
    :type make: str
    :param model: model of car
    :type model: str
    :return: Tuple with formatted make and model
    :rtype: Tuple[str, str]
    """
    url = f"https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/{make}?format=json"

    r = requests.get(url)
    r.raise_for_status()

    data = r.json()
    matched: List[Tuple[str, str]] = [
        (r.get("Make_Name", "").title(), r.get("Model_Name"))
        for r in data.get("Results", [])
        if r.get("Model_Name").lower() == model.lower()
    ]

    if not matched:
        raise ValidationError(
            detail=f"Car not found in NHTSA database (make: {make}, model: {model})"
        )

    return matched[0]
