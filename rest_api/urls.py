from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from rest_api import views

app_name = "rest_api"
urlpatterns = [
    path("cars/", views.CarList.as_view()),
    path("cars/<int:pk>/", views.CarDetail.as_view()),
    path("rate/", views.rate_post_view),
    path("popular/", views.popular_get_view),
]

urlpatterns = format_suffix_patterns(urlpatterns)
