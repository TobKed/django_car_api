"""
WSGI config for django_car_api project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os
import sys
from pathlib import Path

from django.core.wsgi import get_wsgi_application

ROOT_DIR = Path(__file__).resolve(strict=True).parent
sys.path.append(str(ROOT_DIR / "django_car_api"))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_car_api.settings.production")

application = get_wsgi_application()
